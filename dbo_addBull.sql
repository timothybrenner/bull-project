CREATE PROC dbo.addBull
	@CODENO		NVARCHAR(100) = NULL,
	@CODENAME	NVARCHAR(100) = NULL,
	@PTAM		INT = NULL,
	@PTAP		FLOAT = NULL,
	@PTAT		FLOAT = NULL,
	@TPI		INT = NULL,
	@Pedigree	NVARCHAR(MAX) = NULL,
	@UID		NVARCHAR(100) = NULL,
	@RegName	NVARCHAR(MAX) = NULL
AS

SET NOCOUNT ON;

-- Determines if the record already exists, based on the UID.  If it does, it updates the pertinent data.  
-- If it does not exist, it inserts it into the dbo.Bulls table.
IF EXISTS 
(	
	SELECT 
		*
	FROM
		dbo.Bulls
	WHERE
		UID = @UID
)
BEGIN
	UPDATE
		dbo.Bulls
	SET
		CODENO = @CODENO,
		CODENAME = @CODENAME,
		PTAM = @PTAM,
		PTAP = @PTAP,
		PTAT = @PTAT,
		TPI = @TPI,
		Pedigree = @Pedigree,
		RegName = @RegName
	WHERE
		UID = @UID
END
ELSE
BEGIN
	INSERT INTO
	dbo.Bulls
	(
		CODENO,
		CODENAME,
		PTAM,
		PTAP,
		PTAT,
		TPI,
		Pedigree,
		UID,
		RegName
	)
	VALUES
	(
		@CODENO,
		@CODENAME,
		@PTAM,
		@PTAP,
		@PTAT,
		@TPI,
		@Pedigree,
		@UID,
		@RegName
	);
END


