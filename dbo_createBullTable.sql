USE [BullDB]
GO

/****** Object:  Table [dbo].[Bulls]    Script Date: 11/3/2015 12:44:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Bulls](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CODENO] [nvarchar](100) NULL,
	[CODENAME] [nvarchar](100) NULL,
	[PTAM] [int] NULL,
	[PTAP] [float] NULL,
	[PTAT] [float] NULL,
	[TPI] [int] NULL,
	[Pedigree] [nvarchar](max) NULL,
	[UID] [nvarchar](100) NULL,
	[RegName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Bulls] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


