﻿namespace JSONtoMSSQLConverter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFileLocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.browseButton = new System.Windows.Forms.Button();
            this.importJSON = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblDataError = new System.Windows.Forms.Label();
            this.lblSuccessfullyImported = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtFileLocation
            // 
            this.txtFileLocation.Location = new System.Drawing.Point(131, 40);
            this.txtFileLocation.Name = "txtFileLocation";
            this.txtFileLocation.ReadOnly = true;
            this.txtFileLocation.Size = new System.Drawing.Size(410, 20);
            this.txtFileLocation.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "JSON File to Import";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(568, 36);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(75, 23);
            this.browseButton.TabIndex = 2;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // importJSON
            // 
            this.importJSON.Location = new System.Drawing.Point(663, 36);
            this.importJSON.Name = "importJSON";
            this.importJSON.Size = new System.Drawing.Size(75, 23);
            this.importJSON.TabIndex = 3;
            this.importJSON.Text = "Import";
            this.importJSON.UseVisualStyleBackColor = true;
            this.importJSON.Click += new System.EventHandler(this.importJSON_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(512, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "This program is used to open a JSON list of bulls and import them into the Bulls " +
    "table in the SQL Database.  ";
            this.label2.Click += new System.EventHandler(this.label2_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(453, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Duplicate records that are imported will update the existing record based on the " +
    "UID of the bull.";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // lblDataError
            // 
            this.lblDataError.AutoSize = true;
            this.lblDataError.ForeColor = System.Drawing.Color.Red;
            this.lblDataError.Location = new System.Drawing.Point(19, 183);
            this.lblDataError.Name = "lblDataError";
            this.lblDataError.Size = new System.Drawing.Size(197, 13);
            this.lblDataError.TabIndex = 6;
            this.lblDataError.Text = "*File data is either corrupted or incorrect.";
            this.lblDataError.Visible = false;
            this.lblDataError.Click += new System.EventHandler(this.lblDoesNotExist_Click);
            // 
            // lblSuccessfullyImported
            // 
            this.lblSuccessfullyImported.AutoSize = true;
            this.lblSuccessfullyImported.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblSuccessfullyImported.Location = new System.Drawing.Point(131, 67);
            this.lblSuccessfullyImported.Name = "lblSuccessfullyImported";
            this.lblSuccessfullyImported.Size = new System.Drawing.Size(135, 13);
            this.lblSuccessfullyImported.TabIndex = 7;
            this.lblSuccessfullyImported.Text = "Bulls successfully imported.";
            this.lblSuccessfullyImported.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(501, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "NOTE: After clicking \'Import\', if nothing appears to happen wait 30 seconds.  An " +
    "error may have occured.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 394);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblSuccessfullyImported);
            this.Controls.Add(this.lblDataError);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.importJSON);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFileLocation);
            this.Name = "Form1";
            this.Text = "Bull Importer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFileLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Button importJSON;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblDataError;
        private System.Windows.Forms.Label lblSuccessfullyImported;
        private System.Windows.Forms.Label label4;

    }
}

