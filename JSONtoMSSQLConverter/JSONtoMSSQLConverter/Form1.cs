﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace JSONtoMSSQLConverter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            txtFileLocation.Text = openFileDialog1.FileName;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void importJSON_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.CheckFileExists)
            {
                ImportBulls(openFileDialog1.FileName);
            }
        }

        private void lblDoesNotExist_Click(object sender, EventArgs e)
        {

        }

        private void ImportBulls(string fileURL)
        {
            try
            {
                //This reads in the list of bulls from the JSON file and places them in an object.
                RootObject bulls;
                using (StreamReader r = new StreamReader(fileURL))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    bulls = (RootObject)serializer.Deserialize(r, typeof(RootObject));
                }

                //Gets the list of bulls from the JSON object.
                var bullList = bulls.results;

                foreach (var bull in bullList)
                {
                    using (SqlConnection myConnection = new SqlConnection("user id=X\\Tim;" + 
                                       "password=meanbean18;server=X\\SQLEXPRESS;" + 
                                       "Trusted_Connection=yes;" + 
                                       "database=BullDB; " + 
                                       "connection timeout=30"))
                    {
                        using (SqlCommand cmd = new SqlCommand("dbo.addBull", myConnection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add("@CODENO", SqlDbType.NVarChar).Value = bull.codeno;
                            cmd.Parameters.Add("@CODENAME", SqlDbType.NVarChar).Value = bull.codename;

                            int ptam;
                            double ptap;
                            double ptat;
                            int tpi;

                            if(Int32.TryParse(bull.ptam.ToString(), out ptam ))
                            {
                                cmd.Parameters.Add("@PTAM", SqlDbType.Int).Value = ptam;
                            }

                            if (Double.TryParse(bull.ptap.ToString(), out ptap))
                            {
                                cmd.Parameters.Add("@PTAP", SqlDbType.Float).Value = ptap;
                            }

                            if (Double.TryParse(bull.ptat.ToString(), out ptat))
                            {
                                cmd.Parameters.Add("@PTAT", SqlDbType.Float).Value = ptat;
                            }

                            if (Int32.TryParse(bull.tpi.ToString(), out tpi))
                            {
                                cmd.Parameters.Add("@TPI", SqlDbType.Int).Value = tpi;
                            }

                            cmd.Parameters.Add("@Pedigree", SqlDbType.NVarChar).Value = bull.pedigree;
                            cmd.Parameters.Add("@UID", SqlDbType.NVarChar).Value = bull.uid;
                            cmd.Parameters.Add("@RegName", SqlDbType.NVarChar).Value = bull.regname;

                            myConnection.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                }

                lblDataError.Visible = false;
                lblSuccessfullyImported.Visible = true;
            }
            catch (Exception e)
            {
                lblDataError.Visible = true;
                lblDataError.Text = "*" + e.ToString();
                lblSuccessfullyImported.Visible = false;
            }
        }


        public class Result
        {
            public double ptat { get; set; }
            public string pedigree { get; set; }
            public object ptap { get; set; }
            public string uid { get; set; }
            public object tpi { get; set; }
            public string codeno { get; set; }
            public object ptam { get; set; }
            public double id { get; set; }
            public string regname { get; set; }
            public string codename { get; set; }
            public string newstring { get; set; }
        }

        public class RootObject
        {
            public List<Result> results { get; set; }
        }
    }
}
