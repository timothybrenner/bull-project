﻿var app = angular.module("BullDirectoryApp", [
    'ui.grid',
    'ui.grid.exporter',
    'ui.grid.saveState',
    'LocalStorageModule',
    'ngRoute'
])
    .config(['$locationProvider', '$routeProvider', 'localStorageServiceProvider',
        function ($locationProvider, $routeProvider, localStorageServiceProvider) {

        $routeProvider.when('/', {
                controller: 'BullController',
                templateUrl: '/Home/Home'
            })
                      .when('/DetailPage', {
                controller: 'DetailController',
                templateUrl: '/Home/DetailPage'
            })
                      .otherwise({ redirectTo: '/' });

        // Specify HTML5 mode (using the History APIs) or HashBang syntax.
        $locationProvider.html5Mode(false).hashPrefix('!');

        //Used to store the search criteria across multiple sessions.
        localStorageServiceProvider.setPrefix('BullDirectoryApp');
    }])
;

app.controller("BullController", function ($scope, $http, $location, $rootScope, $timeout, localStorageService) {

    //This makes the grid cells clickable, and call a method to redirect to the proper bull's info page.
    var bullGridCellTemplate = '<div class="ui-grid-cell-contents" style="text-decoration:underline">' +
        '<a href ng-click="grid.appScope.clickedCell(row.entity.CODENO)">{{COL_FIELD}}</a>' +
        '</div>';

    $scope.clickedCell = function (codeno) {
        //Stores the selected bull's Code Number then redirects to the proper bull's info page.
        $rootScope.bullCodeNo = codeno;
        $location.path('/DetailPage');
    };  

    //This is an off-the-shelf grid I found called ui-grid.
    $scope.gridOptions = {
        enableFiltering: true,
        enableGridMenu: true,
        saveFilter: true,
        saveSort: true,
        gridMenuShowHideColumns: false,
        exporterMenuVisibleData: true,
        exporterCsvFilename: 'BullList.csv',
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
        exporterPdfDefaultStyle: { fontSize: 9 },
        exporterPdfTableStyle: { margin: [30, 30, 30, 30] },
        exporterPdfTableHeaderStyle: { fontSize: 10, bold: true, italics: true, color: 'red' },
        exporterPdfHeader: { text: "Bull List", style: 'headerStyle' },
        exporterPdfFooter: function (currentPage, pageCount) {
            return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        },
        exporterPdfCustomFormatter: function (docDefinition) {
            docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
            docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
            return docDefinition;
        },
        exporterPdfOrientation: 'landscape',
        exporterPdfPageSize: 'LETTER',
        exporterPdfMaxGridWidth: 500,
        onRegisterApi: function (gridApi) {
            $rootScope.gridApi = gridApi;
        },
        columnDefs: [
            {
                cellTemplate: bullGridCellTemplate, field: 'CODENO', displayName: "Code Number", enableHiding: false
            },
            {
                cellTemplate: bullGridCellTemplate, field: 'CODENAME', displayName: "Code Name", enableHiding: false
            },
            {
                cellTemplate: bullGridCellTemplate, field: 'PTAM', displayName: "PTA Milk (lbs) ", enableHiding: false
            },
            {
                cellTemplate: bullGridCellTemplate, field: 'PTAP', displayName: "PTA Protein (lbs)", enableHiding: false
            },
            {
                cellTemplate: bullGridCellTemplate, field: 'PTAT', displayName: "PTAT", enableHiding: false
            },
            {
                cellTemplate: bullGridCellTemplate, field: 'TPI', displayName: "TPI", enableHiding: false
            }
        ]
    };

    //These store and restore the search credentials.
    $scope.saveState = function () {
        var state = $rootScope.gridApi.saveState.save();
        localStorageService.set('gridState', state);
    }

    $scope.restoreState = function () {
        $timeout(function () {
            var state = localStorageService.get('gridState');
            $rootScope.gridApi.saveState.restore($scope, state);
        });
    }

    $http.get('/api/Values').
        success(function (data) {
            $scope.gridOptions.data = data;
        }).error(function (data) {
            alert("No data to retrieve.");
        });
});

app.controller("DetailController", function ($scope, $http, $rootScope, $location) {

    $http.get('/api/Details?codeno='+ $rootScope.bullCodeNo).
        success(function (data) {
            $scope.bullData = data;
        }).error(function (data) {
            alert("No data to retrieve.");
        });

    //Redirects back to the main bull directory page.
    $scope.backClick = function() {
        $location.path('/');
    };
});

