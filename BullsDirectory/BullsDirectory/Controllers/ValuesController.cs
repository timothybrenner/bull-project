﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BullsDirectory.Models;

namespace BullsDirectory.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values

        public List<BullModelSmall> Get()
        {
            var bullList = new List<BullModelSmall>();
            using (var context = new BullDBEntities())
            {
                var bulls = context.Bulls;
                foreach (var bull in bulls)
                {
                    bullList.Add(new BullModelSmall()
                    {
                        CODENAME = bull.CODENAME,
                        CODENO = bull.CODENO,
                        PTAM = bull.PTAM,
                        PTAP = bull.PTAP,
                        PTAT = bull.PTAT,
                        TPI = bull.TPI
                    });
                }
            }
            //return new string[] { "value1", "value2" };
            return bullList;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}