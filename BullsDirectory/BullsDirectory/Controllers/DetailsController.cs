﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI;
using BullsDirectory.Models;

namespace BullsDirectory.Controllers
{
    public class DetailsController : ApiController
    {
        //
        // GET: /Details/

        public Bull Get(string codeno)
        {
            //This code gets the information on a single bull based on the Code Number.
            var bullToReturn = new Bull();

            using (var context = new BullDBEntities())
            {
                bullToReturn = context.Bulls.FirstOrDefault(x => x.CODENO == codeno);
            }
            return bullToReturn;
        }
    }
}
