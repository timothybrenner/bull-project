﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BullsDirectory.Models
{
    public class BullModelSmall
    {
        public string CODENO { get; set; }
        public string CODENAME { get; set; }
        public Nullable<int> PTAM { get; set; }
        public Nullable<double> PTAP { get; set; }
        public Nullable<double> PTAT { get; set; }
        public Nullable<int> TPI { get; set; }
    }
}